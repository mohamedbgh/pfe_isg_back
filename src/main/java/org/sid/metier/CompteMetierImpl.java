package org.sid.metier;

import java.util.Date;

import org.sid.dao.CompteRepository;
import org.sid.entities.Compte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompteMetierImpl implements CompteMetier{

    @Autowired
    private CompteRepository compteRepository;
    
    
    @Override
    public Compte saveCompte( Compte cp ) {
        cp.setDateCreation( new Date() );
        return compteRepository.save( cp );
    }

    @Override
    public Compte getCompte(String codeCompte ) {
         
        // retourner le code
        return compteRepository.findById( codeCompte ).get();
    }

    @Override
	public Compte consulterCompte(String codeCpte) {
		Compte cp =compteRepository.findById(codeCpte).get();
		if(cp==null) throw new RuntimeException("Compte introuvable");
		return null;
	}

}