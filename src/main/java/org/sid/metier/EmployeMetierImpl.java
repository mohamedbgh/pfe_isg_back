package org.sid.metier;

import java.util.List;

import org.sid.dao.EmployeRepository;
import org.sid.entities.Employes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author MASSI
 *
 */
@Service
public class EmployeMetierImpl implements EmployeMetier {

    @Autowired
    private EmployeRepository employeRepository;

    
    @Override
    public Employes saveEmploye( Employes e ) {
        // TODO Auto-generated method stub
        return employeRepository.save( e );
    }

    @Override
    public List<Employes> listeEmploye() {
        // TODO Auto-generated method stub
        return employeRepository.findAll();
    }
    

}