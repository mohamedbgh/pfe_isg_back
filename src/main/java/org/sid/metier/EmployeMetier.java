package org.sid.metier;

import java.util.List;

import org.sid.entities.Employes;


public interface EmployeMetier {
    
    public Employes saveEmploye(Employes e);
    public List<Employes> listeEmploye();

}
