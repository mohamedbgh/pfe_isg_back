package org.sid.dao;

import org.sid.entities.Employes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeRepository extends JpaRepository<Employes, Long> {

}