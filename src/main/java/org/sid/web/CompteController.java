package org.sid.web;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.CompteRepository;

import  org.sid.entities.Employes;
import  org.sid.entities.Client;
import  org.sid.entities.Compte;

import java.sql.Date;

@RestController
@RequestMapping("/Compte")
public class CompteController {
	private final Logger log = LoggerFactory.getLogger(CompteController.class);

	@Autowired
	private CompteRepository Compterepos ;
	
	@RequestMapping("/allCompte")
	private Collection<Compte> allCompte()
	{
		return Compterepos.findAll();
	}
	@GetMapping("/Compte/{codeCompte}")
    public ResponseEntity<?> getCompteById(@PathVariable("codeCompte") String codeCompte) {
        Optional<Compte> Compte = Compterepos.findById(codeCompte);
        return Compte.map(response -> ResponseEntity.ok().body(response)).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

	
	@GetMapping("/Comptebydatecreation/{datecreation}")
    public Collection<Compte> getCompteBydatecreation(@PathVariable Date datecreation) {
        return Compterepos.findAll().stream().
                filter(x -> x.getDateCreation().equals(datecreation))
                .collect(Collectors.toList());
    }
	
	
	@GetMapping("/Comptebyclient/{client}")
    public Collection<Compte> getCompteByclient(@PathVariable Client client) {
        return Compterepos.findAll().stream().
                filter(x -> x.getClient().equals(client))
                .collect(Collectors.toList());
    }
	
	
	@GetMapping("/Comptebyemploye/{employe}")
    public Collection<Compte> getCompteByemploye(@PathVariable Employes employe) {
        return Compterepos.findAll().stream().
                filter(x -> x.getEmploye().equals(employe))
                .collect(Collectors.toList());
    }
	
	
	@PutMapping("/updateCompte/{codeCompte}")
    public ResponseEntity<Compte> updateCompte(@Valid @RequestBody Compte Compte, @PathVariable("codeCompte") String codeCompte) {
        log.info("Request for updating new Compte {}", Compte);

        Optional<Compte> CompteOptional = Compterepos.findById(codeCompte);

        

        Compte Compte1 = CompteOptional.get();
        
        
        Compte1.setCodeCompte(Compte.getCodeCompte());
        Compte1.setDateCreation(Compte.getDateCreation());
        Compte1.setSolde(Compte.getSolde());
        Compte1.setClient(Compte.getClient()); 
        Compte1.setEmploye(Compte.getEmploye());
        Compte1.setOperations(Compte.getOperations());
       
        Compte result = Compterepos.save(Compte1);

        
        return ResponseEntity.ok().body(result);

	
	

	}

}