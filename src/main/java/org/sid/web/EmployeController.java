package org.sid.web;

import java.util.Optional;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.EmployeRepository;
import  org.sid.entities.Client;
import org.sid.entities.Employes;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/employees")
public class EmployeController {
	
	private final Logger log = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	private EmployeRepository emprepos;
	
	@RequestMapping("/allemployees")
	@CrossOrigin(origins = "http://localhost:4200")
	private Collection<Employes> allclients()
	{
		return emprepos.findAll();
	}
	
	@PostMapping("/ajoutemployee")
	public Employes createEmployee(@Valid @RequestBody Employes employee) {
		return emprepos.save(employee);
	}
	
	 @DeleteMapping("/removeemploye/{codeEmploye}")
	    //@CrossOrigin(origins = "http://localhost:4200")
	    public ResponseEntity<?> deleteEmploye(@PathVariable("codeEmploye") Long codeEmploye) {
	        log.info("Request for removing employe {}", codeEmploye);
	       
	        emprepos.deleteById(codeEmploye);
	        return ResponseEntity.ok().build();	
	    }
	 
	 @DeleteMapping("/removEemployetByNom/{nomEmploye}")
	    public ResponseEntity<?> deleteEmployeByNom(@PathVariable("nomEmploye") String Nomemploye) {
	        log.info("Request for removing employe {}", Nomemploye);
	        List<Employes> lstemp = emprepos.findAll().stream() 
	                .filter(x -> x.getNomEmploye().equals(Nomemploye)) 
	                .collect(Collectors.toList()); 
	        for (Employes e : lstemp)
	            emprepos.deleteById(e.getCodeEmploye());
	        return ResponseEntity.ok().build();

	    }
		
		@GetMapping("/employebyname/{name}")
	    public Collection<Employes> getemployeByName(@PathVariable String name) {
	        return emprepos.findAll().stream().
	                filter(x -> x.getNomEmploye().equals(name))
	                .collect(Collectors.toList());
	    }
	 @PutMapping("/updateemploye/{codeemploye}")
	    public ResponseEntity<Employes> updateemploye(@Valid @RequestBody Employes employe, @PathVariable("codeemploye") long codeemploye) {
	        log.info("Request for updating new employe {}", employe);

	        Optional<Employes> employeOptional = emprepos.findById(codeemploye);

	        

	        Employes employe1 = employeOptional.get();
	        
	        
	        employe1.setNomEmploye(employe.getNomEmploye());
	        employe1.setGroupes(employe.getGroupes());
	        employe1.setEmployesSup(employe.getEmployesSup());
	        

	       
	        Employes result = emprepos.save(employe1);

	        
	        return ResponseEntity.ok().body(result);
	        
	       }
	 
	
}