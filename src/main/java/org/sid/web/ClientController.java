package org.sid.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.ClientRepository;
import org.sid.entities.Client;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/clients")
public class ClientController {
	private final Logger log = LoggerFactory.getLogger(ClientController.class);

	@Autowired
	private ClientRepository clientrepos ;
	
	@RequestMapping("/allclient")
	@CrossOrigin(origins = "http://localhost:4200")
	private Collection<Client> allclients()
	{
		return clientrepos.findAll();
	}
	
	@PostMapping("/newclient")
	@CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Client> addClient(@Valid @RequestBody Client client) throws URISyntaxException {
        log.info("Request for adding new client {}", client);
        Client result = clientrepos.save(client);
        return ResponseEntity.created(new URI("/newclient" + result.getCode())).body(result);
    }
	
	@GetMapping("/client/{codeclient}")
    public ResponseEntity<?> getClientById(@PathVariable("codeclient") Long codeclient) {
        Optional<Client> client = clientrepos.findById(codeclient);
        return client.map(response -> ResponseEntity.ok().body(response)).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
	
	@GetMapping("/clientbyname/{name}")
    public Collection<Client> getClientByName(@PathVariable String name) {
        return clientrepos.findAll().stream().
                filter(x -> x.getNom().equals(name))
                .collect(Collectors.toList());
    }
	
	@PutMapping("/updateclient/{codeclient}")
    public ResponseEntity<Client> updateClient(@Valid @RequestBody Client client, @PathVariable("codeclient") long codeclient) {
        log.info("Request for updating new client {}", client);

        Optional<Client> clientOptional = clientrepos.findById(codeclient);

        

        Client client1 = clientOptional.get();
        client1.setNom(client.getNom());
        client1.setEmail(client.getEmail());
        client1.setComptes(client.getComptes());

        Client result = clientrepos.save(client1);
        //return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(result);
	}
	
    @DeleteMapping("/removeClient/{codeClient}")
    //@CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> deleteClient(@PathVariable("codeClient") Long codeClient) {
        log.info("Request for removing client {}", codeClient);
       
        clientrepos.deleteById(codeClient);
        return ResponseEntity.ok().build();
    }
    
    @DeleteMapping("/removeClientByNom/{nomClient}")
    public ResponseEntity<?> deleteClientByNom(@PathVariable("nomClient") String Nomclient) {
        log.info("Request for removing client {}", Nomclient);
        List<Client> lstclients = clientrepos.findAll().stream() 
                .filter(x -> x.getNom().equals(Nomclient)) 
                .collect(Collectors.toList()); 
        for (Client c : lstclients)
            clientrepos.deleteById(c.getCode());
        return ResponseEntity.ok().build();

    }
}