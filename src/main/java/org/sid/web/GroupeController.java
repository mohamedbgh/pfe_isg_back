package org.sid.web;

import java.sql.Date;
import java.util.Collection;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.sid.dao.GroupeRepository;
import org.sid.entities.Employes;
import org.sid.entities.Groupe;

import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.LoggerFactory;




@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/groupes")
public class GroupeController {
	

	@Autowired
	private GroupeRepository Grouperepos ;
	
	@RequestMapping("/allGroupe")
	private Collection<Groupe> allGroupe()
	{
		return Grouperepos.findAll();
	}
	@GetMapping("/Groupe/{codeGroupe}")
    public ResponseEntity<?> getGroupeById(@PathVariable("codeGroupe") Long codeGroupe) {
        Optional<Groupe> Groupe = Grouperepos.findById(codeGroupe);
        return Groupe.map(response -> ResponseEntity.ok().body(response)).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
	
	@GetMapping("/GroupebynomGroupe/{nomGroupe}")
    public Collection<Groupe> getGroupeBynomGroupe(@PathVariable double nomGroupe) {
        return Grouperepos.findAll().stream().
                filter(x -> x.getNomGroupe().equals(nomGroupe))
                .collect(Collectors.toList());
    }
	
	
	@GetMapping("/Groupebyemploye/{employe}")
    public Collection<Groupe> getGroupeByemploye(@PathVariable Employes employe) {
        return Grouperepos.findAll().stream().
                filter(x -> x.getEmployes().equals(employe))
                .collect(Collectors.toList());
    }
	
	
	@PutMapping("/updateGroupe/{numeroGroupe}")
    public ResponseEntity<Groupe> updateGroupe(@Valid @RequestBody Groupe Groupe, @PathVariable("numeroGroupe") long numeroGroupe) {
        

        Optional<Groupe> GroupeOptional = Grouperepos.findById(numeroGroupe);

        

        Groupe Groupe1 = GroupeOptional.get();

        Groupe1.setNomGroupe(Groupe.getNomGroupe());
        Groupe1.setEmployes(Groupe.getEmployes());
        
        Groupe result = Grouperepos.save(Groupe1);

        
        return ResponseEntity.ok().body(result);


	}


}