package org.sid.web;

import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.Optional;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sid.dao.EmployeRepository;
import org.sid.dao.OperationRepository;
import org.sid.entities.Client;
import org.sid.entities.Compte;
import org.sid.entities.Employes;
import org.sid.entities.Operation;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/operation")
public class OperationController {

	private final Logger log = LoggerFactory.getLogger(OperationController.class);

	@Autowired
	private OperationRepository Operationrepos ;
	
	@RequestMapping("/allOperation")
	private Collection<Operation> allOperation()
	{
		return Operationrepos.findAll();
	}
	@GetMapping("/Operation/{numeroOperation}")
    public ResponseEntity<?> getOperationById(@PathVariable("numeroOperation") Long numeroOperation) {
        Optional<Operation> Operation = Operationrepos.findById(numeroOperation);
        return Operation.map(response -> ResponseEntity.ok().body(response)).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
	
	
	@GetMapping("/Operationbydatecreation/{datecreation}")
    public Collection<Operation> getOperationBydatecreation(@PathVariable Date datecreation) {
        return Operationrepos.findAll().stream().
                filter(x -> x.getDateOperation().equals(datecreation))
                .collect(Collectors.toList());
    }
	
	
	@GetMapping("/Operationbycomte/{compte}")
    public Collection<Operation> getOperationBycompte(@PathVariable Compte compte) {
        return Operationrepos.findAll().stream().
                filter(x -> x.getCompte().equals(compte))
                .collect(Collectors.toList());
    }
	
	
	@GetMapping("/Operationbyemploye/{employe}")
    public Collection<Operation> getOperationByemploye(@PathVariable Employes employe) {
        return Operationrepos.findAll().stream().
                filter(x -> x.getEmploye().equals(employe))
                .collect(Collectors.toList());
    }
	
	
	@PutMapping("/updateOperation/{numeroOperation}")
    public ResponseEntity<Operation> updateOperation(@Valid @RequestBody Operation Operation, @PathVariable("numeroOperation") long numeroOperation) {
        log.info("Request for updating new Operation {}", Operation);

        Optional<Operation> OperationOptional = Operationrepos.findById(numeroOperation);

        

        Operation Operation1 = OperationOptional.get();
        
        
        Operation1.setNumero(Operation.getNumero());
        Operation1.setDateOperation(Operation.getDateOperation());
        Operation1.setMontant(Operation.getMontant());
        Operation1.setCompte(Operation.getCompte()); 
        Operation1.setEmploye(Operation.getEmploye());
        
        Operation result = Operationrepos.save(Operation1);

        
        return ResponseEntity.ok().body(result);

	
	

	}
}